# TEs-R-analysis

Code providing different graphic representations of the transposable elements of the apricot genome on R.


## Input

- [ ] TSV file with TE information[(example)](https://forgemia.inra.fr/marie-hervine.lefevre/tes-r-analysis/-/tree/main/data/info_reswagMa4S_refTEs_TE_match.tsv) 
- [ ] Output file from TEGRiP pipeline [(example)](https://forgemia.inra.fr/marie-hervine.lefevre/tes-r-analysis/-/tree/main/data/output_TE_gene_v2.0_match.tsv) 
- [ ] Information about SNP [(example)](https://forgemia.inra.fr/marie-hervine.lefevre/tes-r-analysis/-/tree/main/data/SNP_chromplot.txt) 
- [ ] Information about chromosomes [(example)](https://forgemia.inra.fr/marie-hervine.lefevre/tes-r-analysis/-/tree/main/data/chromosome_file.txt) 
- [ ] List of differentially expressed genes [(example)](https://forgemia.inra.fr/marie-hervine.lefevre/tes-r-analysis/-/tree/main/data/DEG) 
